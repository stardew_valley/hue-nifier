name = \[CP\]\ Hue-Nifier
folder = $(name)/
zipfile = $(name).zip

all: zip clean

clean:
	rm -rf $(folder)
	echo "Filesystem cleaned"

zip: files
	rm -f $(zipfile)
	zip -r $(zipfile) $(folder)
	echo "Zip file complete!"

files: folders
	cp LICENCE $(name)
	cp README.md $(name)
	cp manifest.json $(name)
	cp content.json $(name)
	cp -r assets/*.png $(name)"/assets/"
	cp -r simple/*.json $(name)"/simple/"

folders:
	mkdir -p $(name)
	mkdir -p $(name)"/assets"
	mkdir -p $(name)"/simple"
